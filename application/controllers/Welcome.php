<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('javascript');
        $this->load->database();
        
        $this->load->library('calendar');
    }
	public function index()
	{
		
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('password','Password','required');
            if($this->form_validation->run()==FALSE)
            {
              
                $this->load->view('login');
              
            }
            else
            {
                $email=$this->input->post('email');
                $password=$this->input->post('password');
                
                $this->load->model('Userdetails');
                $result=$this->Userdetails->checklogin($email, $password);
                
                   if($result)
                   {
                      $data=array('email'=>$email,'islogin'=>true);
                      $this->session->set_userdata('sessionid',$data);
                      redirect(site_url('/Welcome/profile'));
                   }
                   else
                   {
                      $this->session->set_flashdata('error', 'Enter correct Password and Email');
                      redirect('');
                   }
           }

    
	}
}
