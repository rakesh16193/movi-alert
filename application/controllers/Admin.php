<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->helper('html');
        $this->load->library('session');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->library('encryption');
        $this->load->library('Ajax_pagination');
        $this->perPage = 50;
        
        $this->load->helper('string');  
        if (! $this->session->userdata('sessionid'))
        { 
            $allowed = array(
                'index', 'signup'           
            );
            if(!in_array($this->router->fetch_method(), $allowed))
            {
                redirect('/admin');
            }
        }   
    }
    
    
   
   
       public function index()
       {  
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password','Password','required');
        if($this->form_validation->run()==FALSE)
        {
          
            $this->load->view('loginform');
          
        }
        else
        {
            $email=$this->input->post('email');
            $password=md5($this->input->post('password'));
            
           
            $result=$this->Admin_model->checklogin($email, $password);
            
               if($result)
               {
                  $user_type=$result[0]['user_type'];
                  $data=array('email'=>$email,'islogin'=>true, 'user_type'=>$user_type);
                  $this->session->set_userdata('sessionid',$data);
                  redirect(site_url('/admin/dashboard'));
               }
               else
               {
                  $this->session->set_flashdata('error', 'Enter correct Password and Email');
                  redirect('');
               }
       }
   }
   public function signup()
   {
    $this->form_validation->set_rules('firstname','First name','required');
    $this->form_validation->set_rules('lastname','Last name','required');
    $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users.email]',array('is_unique'=>'email is already registred'));
    $this->form_validation->set_rules('mobile','Mobile','required|regex_match[/^[0-9]{10}$/]');
    $this->form_validation->set_rules('Language','Language','required');
    $this->form_validation->set_rules('password','Password','required');
    $this->form_validation->set_rules('c_password','confirm Password','required|matches[password]');
        if($this->form_validation->run()==FALSE)
    {         
        $this->load->view('signup');         
    }
    else
    {
        $data = array(
            'first_name' => $this->input->post('firstname'),
            'last_name' => $this->input->post('lastname'),
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mobile'),
            'language' => $this->input->post('Language'),
            'password' => md5($this->input->post('c_password'))
        );
        $result = $this->Admin_model->signup($data);
        if($result){
            redirect('/');
        }
        else
        {
            $this->session->set_flashdata('error', 'Enter correct Password and Email');
            redirect('');
        }
    } 
   }
   public function dashboard()
   {
       $this->load->view('dashboard');
   }
   public function logout(){
    $this->session->unset_userdata('sessionid');
    redirect('/');
   }

   public function add_incident(){
    $this->form_validation->set_rules('date','Date','required');
    $this->form_validation->set_rules('time','Time','required');
    $this->form_validation->set_rules('latitude','Latitude','required');
    $this->form_validation->set_rules('longitude','Longitude','required');
    $this->form_validation->set_rules('google_link','Google map link','required');
    $this->form_validation->set_rules('city','City','required');
    $this->form_validation->set_rules('zone','Zone','required');
    $this->form_validation->set_rules('neighborhood','Neighborhood','required');
    $this->form_validation->set_rules('street','Street','required');
    $this->form_validation->set_rules('category_of_incidents','Category','required');
    $this->form_validation->set_rules('security','Security','required');
    $this->form_validation->set_rules('title_en','Title','required');
    $this->form_validation->set_rules('title_pt','Title','required');
    $this->form_validation->set_rules('description_en','Description','required');
    $this->form_validation->set_rules('description_pt','Description','required');
    $this->form_validation->set_rules('link','Link','required');
        if($this->form_validation->run()==FALSE)
    {     
        $city['city']=$this->Admin_model->getcity();   
        $city['category'] =$this->Admin_model->getcategory();
        $this->load->view('add_incident',$city);         
    }
    else
    {
        $data = array(
            'date' => $this->input->post('date'),
            'time' => $this->input->post('time'),
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude'),
            'google_link' => $this->input->post('google_link'),
            'city' => $this->input->post('city'),
            'zone' => $this->input->post('zone'),
            'neighborhood' => $this->input->post('neighborhood'),
            'street' => $this->input->post('street'),
            'category_incident' => $this->input->post('category_of_incidents'),
            'level_security' => $this->input->post('security'),
            'title_en' => $this->input->post('title_en'),
            'title_pt' => $this->input->post('title_pt'),
            'description_en' => $this->input->post('description_en'),
            'description_pt' => $this->input->post('description_pt'),
            'link' => $this->input->post('link'),   

        );  
        $result = $this->Admin_model->add_incident($data);
        if($result){
            $this->session->set_flashdata('success', 'Question added successfully');
			redirect('admin/add_incident');
        }
        else
        {
            $this->session->add_incident('error', 'Something went wrong!!');
            redirect('admin/add_incident');
        }
      } 
   }

   public function getzone(){
    if(!empty($_POST["city_id"])){
    $city_id= $this->input->post('city_id');
        //Fetch all state data
        $sql= "SELECT * FROM zone WHERE city_id = '$city_id' ";
        
        $result= $this->db->query($sql);
        //Count total number of rows
        $rowCount = $result->num_rows();  
        // echo $rowCount;   
        // die($rowCount);
        //State option list
        if($rowCount > 0){
            
            echo '<option value="">Select Zone</option>';

            foreach($result->result_array() as $row){ 
               
                echo '<option value="'.$row['id'].'">'.$row['zone_name'].'</option>';
            }
        }else{
            echo '<option value="">Zone not available</option>';
        }
    }elseif(!empty($_POST["zone_id"])){
        $zone_id= $this->input->post('zone_id');
        //Fetch all state data
        $sql= "SELECT * FROM neighborhood WHERE zone_id = '$zone_id' ";
        
        $result= $this->db->query($sql);
        //Count total number of rows
        $rowCount = $result->num_rows();  
        // echo $rowCount;   
        // die($rowCount);
        //State option list
        if($rowCount > 0){
            
            echo '<option value="">Select neighborhood</option>';

            foreach($result->result_array() as $row){ 
               
                echo '<option value="'.$row['id'].'">'.$row['neighborhood_name'].'</option>';
            }
        }else{
            echo '<option value="">neighborhood not available</option>';
        }

    }
   }
   
    public function getneighborhood(){
        $zone_id= $this->input->post('zone_id');
            //Fetch all state data
            $sql= "SELECT * FROM neighborhood WHERE zone_id = '$zone_id' ";
            
            $result= $this->db->query($sql);
            //Count total number of rows
            $rowCount = $result->num_rows();  
            // echo $rowCount;   
            // die($rowCount);
            //State option list
            if($rowCount > 0){
                
                echo '<option value="">Select neighborhood</option>';
    
                foreach($result->result_array() as $row){ 
                   
                    echo '<option value="'.$row['id'].'">'.$row['neighborhood_name'].'</option>';
                }
            }else{
                echo '<option value="">State not available</option>';
            }
        }

        public function edit()
        {
            $this->load->view('edit');
        }

        public function menu(){
            $data = array();
            
            //total rows count
            $totalRec = count($this->getRows());
            
            //pagination configuration
            $config['target']      = '#postList';
            $config['base_url']    = base_url().'/admin/ajaxPaginationData';
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $this->ajax_pagination->initialize($config);
            
            //get the posts data
            $data['posts'] = $this->getRows(array('limit'=>$this->perPage));
            
            //load the view
            $this->load->view('view_data', $data);
        }
        
        function ajaxPaginationData(){
            $page = $this->input->post('page');
            if(!$page){
                $offset = 0;
            }else{
                $offset = $page;
            }
            
            //total rows count
            $totalRec = count($this->getRows());
            
            //pagination configuration
            $config['target']      = '#postList';
            $config['base_url']    = base_url().'/admin/ajaxPaginationData';
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $this->ajax_pagination->initialize($config);
            
            //get the posts data
            $data['posts'] = $this->getRows(array('start'=>$offset,'limit'=>$this->perPage));
            
            //load the view
            $this->load->view('posts/ajax-pagination-data', $data, false);
        }
      
        function getRows($params = array())
    {
        $this->db->select('*');
        $this->db->from('incident');
        
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }
        
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
}
