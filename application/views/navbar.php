<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MOVI-ALERT Admin APPS</title>
    <link rel="icon" href="http://movisos-admin.movisafe-americalatina.com/movi-sos/favicon.png" type="image/gif">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.11/css/mdb.min.css" rel="stylesheet">


   <!-- JQuery -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.11/js/mdb.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js
"></script> 


<!-- <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script> -->
</head>
<style>
.navbar.navbar-dark .navbar-nav .nav-item.active>.nav-link {
  background-color:#00688B;
}
#menu{
  float:right;
}
</style>
<body style="font-size:85%;">
<header>
          <!-- navbar -->
          <div id="top">
          <nav class="navbar navbar-expand-lg navbar-dark" style="background-color:#021e42;">
             
             <!-- <a class="navbar-brand" href="#">Movi-Sos</a> -->
             <!-- <a href="<?php echo base_url('/');?>"> <img src="<?php echo 'http://movisos-admin.movisafe-americalatina.com/movi-sos/images/LogoMovisafe_Quadri.png'; ?>" width="100" height="50"/></a> -->
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
      <div class="container">
              <div class="collapse navbar-collapse" id="basicExampleNav">
             
                <ul class="navbar-nav  mr-auto">
                <?php 
              if($this->session->has_userdata('sessionid'))

              {
                $type=$this->session->userdata['sessionid']['email'];

                ?>
               <!-- <li class="nav-item ">
                 <a class="nav-link " href="<?php echo base_url('admin/dashboard'); ?>">MOVI SOS <span class="sr-only">(current)</span></a>
               </li> -->
               <li class="nav-item ">
                 <a class="nav-link" href="<?php echo base_url('admin/add_incident'); ?>">Add Incidents</a>
               </li>
               
               <li class="nav-item ">
                 <a class="nav-link" data-toggle="modal" data-target="#basicExampleModal">Logout</a>
               </li>
             </ul>
             <ul class="navbar-nav  mr-auto"></ul>
             <ul class="navbar-nav  mr-right">
              <li class="nav-item ">
                 <a class="nav-link"><?php echo $type;?></a>
               </li>
                 <!-- <li class="nav-item dropdown">
                  <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="<?php echo base_url('admin/adduserdata'); ?>">Add User</a>
                  <a class="dropdown-item" href="<?php echo base_url('admin/totaladminuser'); ?>">Admin User List</a>
                  </div>
                </li> -->
                <?php
                }
                ?>
             </ul>
             </div>
          </div>
                
         </nav>
          </div>
          
</header>
    

<!-- logout Modal -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm logout</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
        <a class="btn btn-indigo btn-sm" href="<?php echo base_url('admin/logout') ?>">Logout</a>
      </div>
    </div>
  </div>
</div>
