<?php 
$path=base_url('/admin/dashboard');
include("navbar.php");
?>
<div id="content-wrapper">
<div class="container-fluid">
<div class="row" style="margin-bottom: 10px;">
<div class="col-md-6" style="margin-top: 80px;" >
<div class="post-search-panel">
<input type="text" id="keywords" class="form-control" placeholder="Type keywords to filter posts" onkeyup="searchFilter()"/> 
</div>
</div>
<div class="col-md-6" id="mysearch" style="margin-top: 80px;">
<select id="sortBy" onchange="searchFilter()" class="form-control">
<option value="">Sort By</option> 
<option value="date_asc">Date Ascending</option>
<option value="date_desc">Date Descending</option>
<option value="city_asc">City Ascending</option>
<option value="city_desc">City Descending</option> 
<option value="zone_asc">Zone Ascending</option>
<option value="zone_desc">Zone Descending</option> 
<option value="neighborhood_asc">Neighborhood Ascending</option>
<option value="neighborhood_desc">Neighborhood Descending</option> 
<option value="incident_asc">Incident type Ascending</option>
<option value="incident_desc">Incident type Descending</option> 
<option value="security_asc">Security level Ascending</option>
<option value="security_desc">Security level Descending</option> 
</select>
</div>
</div>
<div class="row"> 
<div class="col-md-12" >
<div class="post-search-panel">
<?php
$this->load->view('ajax_pagination_data.php');
?>
</div> 
</div>
</div> 
</div> 
</div> 
<script>
function searchFilter(page_num) {
page_num = page_num?page_num:0;
var keywords = $('#keywords').val();
var sortBy = $('#sortBy').val();
$.ajax({
type: 'POST',
url: '<?php echo base_url(); ?>/admin/ajaxPaginationData/'+page_num,
data:'page='+page_num+'&keywords='+keywords+'&sortBy='+sortBy,
beforeSend: function () {
$('.loading').show();
},
success: function (html) {
$('#postList').html(html);
var count = page_num+1;
var total = 50;
var i = 1;
for(i = 1;i<=total;i++){
$('#srno'+i).html(count++);
}
$('.loading').fadeOut("slow");
}
});
}

</script> 

<?php
include('footer.php');
?>