<?php
include('navbar.php');
?>
<div class="container" style="margin-top:30px;">
    <div class="row">   
        <div class="col-md-12">
          <?php if($this->session->flashdata('success')) 
                { 
                ?>
                <h6>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </h6>
                <?php 
                    } 
                 ?>
                <?php if($this->session->flashdata('error')) 
                { 
                ?>
                <h6>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </h6>
                <?php 
                    } 
                 ?>
            <div>
           
            </div>
        <div class="card">
        
                      <h4 class="card-header text-light indigo" style="text-align:center;">Update Incidents</h4>
					 <div class="card-body">
                     <form method="post">
                                <div class="form-row">
                                        <div class="form-group col-md-4">
                                           <label>Date:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-calendar text-indigo"></i></div>
                                                </div>
                                              <input type="date" class="form-control" name="date" placeholder="Enter date" value="<?php echo set_value('date');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('date');  ?>
                                                </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                           <label>Time:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-clock-o text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="time" placeholder="Enter time" value="<?php echo set_value('time');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('time');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                           <label>Latitude:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-globe text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="latitude" placeholder="Enter latitude" value="<?php echo set_value('latitude');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('latitude');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col-md-4">
                                           <label>Longitude:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-globe text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="longitude" placeholder="Enter longitude" value="<?php echo set_value('longitude');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('longitude');  ?>
                                                </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                           <label>Google map link:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-link text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="google_link" placeholder="Enter google link" value="<?php echo set_value('google_link');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('google_link');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                        <label>City:</label>
                                            <select name="city"  class="form-control" id="city" >
                                            <option value="">---Select City---</option>
                                            <?php

                                                            foreach ($city->result() as $key)
                                                            {
                                                         ?>
                                                            <option value="<?php echo $key->id;?>" <?php echo set_select('city',$key->id); ?>><?php echo $key->city_name;?><?php echo set_value($key->city_name);?></option>
                                                        <?php   
                                                            }
                                                        ?>
                                            </select>
                                                <div class="text-danger">
                                                <?php echo form_error('city');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col-md-4">
                                        <label>Zone:</label>
                                            <select name="zone"  class="form-control" id="zone" >
                                                <option value="">Select City first</option>
                                                
                                            </select>
                                                <div class="text-danger">
                                                <?php echo form_error('zone');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                        <label>Neighborhood:</label>
                                            <select name="neighborhood"  class="form-control" id="neighborhood">
                                                <option value="">Select Zone first</option>
                                                
                                            </select>
                                                <div class="text-danger">
                                                <?php echo form_error('neighborhood');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                           <label>Street:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-street-view text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="street" placeholder="Enter street " value="<?php echo set_value('street');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('street');  ?>
                                                </div>
                                        </div>
                                </div>
                               
                                <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label>Category of incident:</label>
                                            <select name="category_of_incidents"  class="form-control" >
                                                <option value="">---Select Category of incident:---</option>
                                                <?php

                                                            foreach ($category->result() as $key)
                                                            {
                                                         ?>
                                                            <option value="<?php echo $key->id;?>" <?php echo set_select('category_of_incidents',$key->incident_category); ?>><?php echo $key->incident_category;?><?php echo set_value($key->incident_category);?></option>
                                                        <?php   
                                                            }
                                                        ?>
                                            </select>
                                                <div class="text-danger">
                                                <?php echo form_error('category_of_incidents');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                           <label>Level of security:</label>
                                            <select name="security"  class="form-control" >
                                                <option value="">---Select Level of security---</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                                <div class="text-danger">
                                                <?php echo form_error('security');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                           <label>Title EN:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-file-text text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="title_en" placeholder="Enter title " value="<?php echo set_value('title_en');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('title_en');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col-md-4">
                                           <label>Title PT:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-file-text text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="title_pt" placeholder="Enter title " value="<?php echo set_value('title_pt');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('title_pt');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                           <label>Description EN:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-file-text text-indigo"></i></div>
                                                </div>
                                              <textarea rows="1" class="form-control" name="description_en" placeholder="Enter Description " ></textarea>
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('description_en');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                           <label>Description PT:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-file-text text-indigo"></i></div>
                                                </div>
                                                <textarea rows="1" class="form-control" name="description_pt" placeholder="Enter Description "></textarea>                                
                                                 </div>
                                                <div class="text-danger">
                                                <?php echo form_error('description_pt');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col-md-4">
                                           <label>Link:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-link text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="link" placeholder="Enter link " value="<?php echo set_value('link');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('link');  ?>
                                                </div>
                                        </div>
                                 </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-indigo btn-sm">Add-Incident</button>
                                </div>
                            </form>
    </div>
    </div><br>
    
</div>
</div>

<?php
include('footer.php');
?>

<script type="text/javascript">
$(document).ready(function(){
    $('#city').on('change',function(){
        var city_id = $(this).val();
        if(city_id){
            $.ajax({
                type:'POST',
                url:'http://localhost/projects/movi-alert/index.php/admin/getzone',
                data:'city_id='+city_id,
                success:function(html){
                    $('#zone').html(html);
                    $('#neighborhood').html('<option value="">Select Zone first</option>'); 
                }
            }); 
        }else{
            $('#zone').html('<option value="">Select city first</option>');
            $('#neighborhood').html('<option value="">Select zone first</option>'); 
        }
    });
    
    $('#zone').on('change',function(){
        var zone_id = $(this).val();
        if(zone_id){
            $.ajax({
                type:'POST',
                url:'http://localhost/projects/movi-alert/index.php/admin/getzone',
                data:'zone_id='+zone_id,
                success:function(html){
                    $('#neighborhood').html(html);
                  //  $('#neighborhood').html('<option value="">Select Zone first</option>'); 

                }
            }); 
        }else{
            $('#neighborhood').html('<option value="">Select Zone first</option>'); 

        }
    });
});
</script>