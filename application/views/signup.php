<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <link rel="icon" href="http://movisos-admin.movisafe-americalatina.com/movi-sos/favicon.png" type="image/gif">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Font Awesome -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.11/css/mdb.min.css" rel="stylesheet">
   <!-- JQuery -->
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.11/js/mdb.min.js"></script>
  
</head>
<style>
/* body {
    
        background-image: url("http://movisos-admin.movisafe-americalatina.com/movi-sos/images/CouvertureMOVISAFE.jpg");
      
}  */
.banner{
background-image: url("http://movisos-admin.movisafe-americalatina.com/movi-sos/images/CouvertureMOVISAFE.jpg");
  position:absolute;
  top:0;
  left:0;
  right:0;
  bottom:0;
  background-size:cover;
  background-position: center;
}
.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
   
}
.card { background-color: transparent; }
.card-header, .card-footer { opacity: 1}
.card-body label{ color: white;}
</style>
<body>
<div class="banner"> 
<div class="container" style="margin-top:50px;">
    <div class="row">
        <div class="col-md-2">
         </div>
        
        <div class="col-md-8">
           <?php if ($this->session->flashdata('error')) 
           { 
           ?>
                <h6>
                    <div class= "alert alert-danger alert-dismissible fade show" role="alert"><?php echo $this->session->flashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                    </div>
                </h6>
            <?php 
            } 
            ?>
            <div>
           
            </div>
        <div class="card" style="margin-top:10px;">
        
                      <h4 class="card-header text-light indigo" style="text-align:center;">Sign-Up</h4>
                      <!-- <img src="<?php echo 'http://movisos-admin.movisafe-americalatina.com/movi-sos/images/LogoMovisafe_Quadri.png'; ?>" width="100" height="60" class="center"/> -->
					 <div class="card-body">
                     <form method="post">
                                <div class="form-row">
                                        <div class="form-group col-md-6">
                                           <label>First Name:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-user text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="firstname" placeholder="Enter your first name" value="<?php echo set_value('firstname');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('firstname');  ?>
                                                </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                           <label> Last Name:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-user text-indigo"></i></div>
                                                </div>
                                              <input type="text" class="form-control" name="lastname" placeholder="Enter your last name" value="<?php echo set_value('lastname');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('lastname');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col-md-6">
                                           <label> Email:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-envelope text-indigo"></i></div>
                                                </div>
                                              <input type="email" class="form-control" name="email" placeholder="Enter your email" value="<?php echo set_value('email');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('email');  ?>
                                                </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                           <label> Mobile No:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-phone  text-indigo"></i></div>
                                                </div>
                                              <input type="number" class="form-control" name="mobile" placeholder="Enter your mobile number" value="<?php echo set_value('mobile');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('mobile');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                        <div class="form-group col-md-6">
                                           <label>Password:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-unlock-alt text-indigo"></i></div>
                                                </div>
                                              <input type="password" class="form-control" name="password" placeholder="Enter password " value="<?php echo set_value('password');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('password');  ?>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                           <label>Confirm-Password:</label>
                                            <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-unlock-alt text-indigo"></i></div>
                                                </div>
                                              <input type="password" class="form-control" name="c_password" placeholder="Confirm password " value="<?php echo set_value('c_password');?>">
                                            </div>
                                                <div class="text-danger">
                                                <?php echo form_error('c_password');  ?>
                                                </div>
                                        </div>
                                </div>
                                <div class="form-row">
                                    <label for="">Language:</label>
                                    <select name="Language" class="form-control">
                                    <option value="">---Select Language---</option>

                                        <option value="en">English</option>
                                        <option value="br">Portugal</option>
                                    </select>
                                    <div class="text-danger">
                                                <?php echo form_error('Language');  ?>
                                                </div>
                                </div><br>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-indigo btn-sm">sign-up</button>
                                </div>
                            </form>
    </div>
    </div>
    <div class="col-md-2">
         </div>
</div>
</div>
</div>
</body>
</html>
  